var interface_gallery_1_1_repository_1_1_i_person_repository =
[
    [ "AddPerson", "interface_gallery_1_1_repository_1_1_i_person_repository.html#aaea9cb28d8a294178a62d8adb0ac3d1a", null ],
    [ "ChangeEmail", "interface_gallery_1_1_repository_1_1_i_person_repository.html#a543c206275842f84ec374a1908000d2b", null ],
    [ "ChangePhoneNumber", "interface_gallery_1_1_repository_1_1_i_person_repository.html#aacf546bef8c9fccce309310091976d82", null ],
    [ "ChangeZipCode", "interface_gallery_1_1_repository_1_1_i_person_repository.html#ad2a9f4d14ea886d413ffeab93731cf8f", null ],
    [ "DeletePerson", "interface_gallery_1_1_repository_1_1_i_person_repository.html#a3254be3906d8eb607d924eb70af9f324", null ]
];