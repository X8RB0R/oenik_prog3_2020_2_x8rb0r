var namespace_gallery_1_1_logic =
[
    [ "Classes", "namespace_gallery_1_1_logic_1_1_classes.html", "namespace_gallery_1_1_logic_1_1_classes" ],
    [ "Interfaces", "namespace_gallery_1_1_logic_1_1_interfaces.html", "namespace_gallery_1_1_logic_1_1_interfaces" ],
    [ "QueryGroups", "namespace_gallery_1_1_logic_1_1_query_groups.html", "namespace_gallery_1_1_logic_1_1_query_groups" ],
    [ "Tests", "namespace_gallery_1_1_logic_1_1_tests.html", "namespace_gallery_1_1_logic_1_1_tests" ],
    [ "IPersonLogic", "interface_gallery_1_1_logic_1_1_i_person_logic.html", "interface_gallery_1_1_logic_1_1_i_person_logic" ],
    [ "PersonLogic", "class_gallery_1_1_logic_1_1_person_logic.html", "class_gallery_1_1_logic_1_1_person_logic" ]
];