var interface_gallery_1_1_logic_1_1_i_person_logic =
[
    [ "AddPerson", "interface_gallery_1_1_logic_1_1_i_person_logic.html#a517a3614e617f2cef1e3561248be7841", null ],
    [ "ChangeEmail", "interface_gallery_1_1_logic_1_1_i_person_logic.html#a5672fab082b7e2c622c72b3b770cf6a5", null ],
    [ "ChangePhoneNumber", "interface_gallery_1_1_logic_1_1_i_person_logic.html#a2a9f5abf4c5f47f153803bfd8cd5d713", null ],
    [ "ChangeZipCode", "interface_gallery_1_1_logic_1_1_i_person_logic.html#ae7984c806629438b653ceadcfe7bfd53", null ],
    [ "DeletePerson", "interface_gallery_1_1_logic_1_1_i_person_logic.html#a0df9dacd6da5a1c0f4b645df61a0c151", null ],
    [ "GetAllPeople", "interface_gallery_1_1_logic_1_1_i_person_logic.html#a50ca7a1ddad406e5ca1d3b4320487ec9", null ],
    [ "GetPersonById", "interface_gallery_1_1_logic_1_1_i_person_logic.html#a8814d201c99185c581e94b490d46aff3", null ],
    [ "PersonExists", "interface_gallery_1_1_logic_1_1_i_person_logic.html#a42fc2608170e05b206f93db8b80faff4", null ]
];