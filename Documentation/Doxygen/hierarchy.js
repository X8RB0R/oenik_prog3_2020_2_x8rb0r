var hierarchy =
[
    [ "OENIK_PROG3_2020_2_X8RB0R.Application", "class_o_e_n_i_k___p_r_o_g3__2020__2___x8_r_b0_r_1_1_application.html", null ],
    [ "Gallery.Program.ConsoleCommands", "class_gallery_1_1_program_1_1_console_commands.html", null ],
    [ "DbContext", null, [
      [ "Gallery.Data.GalleryContext", "class_gallery_1_1_data_1_1_gallery_context.html", null ]
    ] ],
    [ "Exception", null, [
      [ "Gallery.Repository.Exceptions.InvalidChangeException", "class_gallery_1_1_repository_1_1_exceptions_1_1_invalid_change_exception.html", null ],
      [ "Gallery.Repository.Exceptions.NoInstanceFoundException", "class_gallery_1_1_repository_1_1_exceptions_1_1_no_instance_found_exception.html", null ]
    ] ],
    [ "Gallery.Data.Models.Exhibit", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html", null ],
    [ "Gallery.Logic.QueryGroups.ExhibitsAndCountPaintingsGroup", "class_gallery_1_1_logic_1_1_query_groups_1_1_exhibits_and_count_paintings_group.html", null ],
    [ "Gallery.Program.Factory", "class_gallery_1_1_program_1_1_factory.html", null ],
    [ "Gallery.Logic.Tests.GalleryLogicTests", "class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests.html", null ],
    [ "Gallery.Repository.GenericRepository< Exhibit >", "class_gallery_1_1_repository_1_1_generic_repository.html", [
      [ "Gallery.Repository.ExhibitRepository", "class_gallery_1_1_repository_1_1_exhibit_repository.html", null ]
    ] ],
    [ "Gallery.Repository.GenericRepository< Painting >", "class_gallery_1_1_repository_1_1_generic_repository.html", [
      [ "Gallery.Repository.PaintingRepository", "class_gallery_1_1_repository_1_1_painting_repository.html", null ]
    ] ],
    [ "Gallery.Repository.GenericRepository< Person >", "class_gallery_1_1_repository_1_1_generic_repository.html", [
      [ "Gallery.Repository.PersonRepository", "class_gallery_1_1_repository_1_1_person_repository.html", null ]
    ] ],
    [ "Gallery.Logic.Interfaces.IGalleryLogic", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html", [
      [ "Gallery.Logic.Classes.GalleryLogic", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html", null ]
    ] ],
    [ "Gallery.Logic.IPersonLogic", "interface_gallery_1_1_logic_1_1_i_person_logic.html", [
      [ "Gallery.Logic.PersonLogic", "class_gallery_1_1_logic_1_1_person_logic.html", null ]
    ] ],
    [ "Gallery.Repository.IRepository< T >", "interface_gallery_1_1_repository_1_1_i_repository.html", [
      [ "Gallery.Repository.GenericRepository< T >", "class_gallery_1_1_repository_1_1_generic_repository.html", null ]
    ] ],
    [ "Gallery.Repository.IRepository< Exhibit >", "interface_gallery_1_1_repository_1_1_i_repository.html", [
      [ "Gallery.Repository.IExhibitRepository", "interface_gallery_1_1_repository_1_1_i_exhibit_repository.html", [
        [ "Gallery.Repository.ExhibitRepository", "class_gallery_1_1_repository_1_1_exhibit_repository.html", null ]
      ] ]
    ] ],
    [ "Gallery.Repository.IRepository< Painting >", "interface_gallery_1_1_repository_1_1_i_repository.html", [
      [ "Gallery.Repository.IPaintingRepository", "interface_gallery_1_1_repository_1_1_i_painting_repository.html", [
        [ "Gallery.Repository.PaintingRepository", "class_gallery_1_1_repository_1_1_painting_repository.html", null ]
      ] ]
    ] ],
    [ "Gallery.Repository.IRepository< Person >", "interface_gallery_1_1_repository_1_1_i_repository.html", [
      [ "Gallery.Repository.IPersonRepository", "interface_gallery_1_1_repository_1_1_i_person_repository.html", [
        [ "Gallery.Repository.PersonRepository", "class_gallery_1_1_repository_1_1_person_repository.html", null ]
      ] ]
    ] ],
    [ "Gallery.Logic.QueryGroups.MostExpensivePaintingAndItsExhibitGroup", "class_gallery_1_1_logic_1_1_query_groups_1_1_most_expensive_painting_and_its_exhibit_group.html", null ],
    [ "Gallery.Data.Models.Painting", "class_gallery_1_1_data_1_1_models_1_1_painting.html", null ],
    [ "Gallery.Logic.QueryGroups.PaintingsInSunshineGroup", "class_gallery_1_1_logic_1_1_query_groups_1_1_paintings_in_sunshine_group.html", null ],
    [ "Gallery.Data.Models.Person", "class_gallery_1_1_data_1_1_models_1_1_person.html", null ],
    [ "Gallery.Logic.Tests.PersonLogicTests", "class_gallery_1_1_logic_1_1_tests_1_1_person_logic_tests.html", null ]
];