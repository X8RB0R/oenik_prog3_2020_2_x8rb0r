var class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests =
[
    [ "AddPainting_Test", "class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests.html#ad633b4ab669c94ef135a90f34b6a914b", null ],
    [ "DeletePainting_Test", "class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests.html#ac69764966ab363f29d420d7870ad2e11", null ],
    [ "ExhibitsAndCountPaintings_Test", "class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests.html#a9bb91e933d54dd6265fddf89e44c3416", null ],
    [ "GetAllExhibits_Test", "class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests.html#aab5b5c5c899b0476fb976c2ee3a4d76b", null ],
    [ "MostExpensivePaintingAndItsExhibit_Test", "class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests.html#a4163cec4a2627befb06f9d2da86129cd", null ],
    [ "PaintingsInSunshineExhibit_Test", "class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests.html#ab4e3aa7015100a785542accf15eff5f7", null ]
];