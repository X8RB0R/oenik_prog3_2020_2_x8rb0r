var class_gallery_1_1_repository_1_1_person_repository =
[
    [ "PersonRepository", "class_gallery_1_1_repository_1_1_person_repository.html#ae4b67f05865978c91b0c5e0bc7f7b4fe", null ],
    [ "AddPerson", "class_gallery_1_1_repository_1_1_person_repository.html#aeac30cf5767ded21ae33e4c8697bb572", null ],
    [ "ChangeEmail", "class_gallery_1_1_repository_1_1_person_repository.html#ab68db190dfe3b241e3c07b7c859ab316", null ],
    [ "ChangePhoneNumber", "class_gallery_1_1_repository_1_1_person_repository.html#a53e05784553e32ce58b28e94be0cf9bc", null ],
    [ "ChangeZipCode", "class_gallery_1_1_repository_1_1_person_repository.html#aa6127ed22937b0e717d288b52456fb7c", null ],
    [ "DeletePerson", "class_gallery_1_1_repository_1_1_person_repository.html#a19aea2ed2ca50dba11c520087a4fe094", null ],
    [ "Exists", "class_gallery_1_1_repository_1_1_person_repository.html#a2e18578917c392487f6cb1e0a3437abc", null ],
    [ "GetOne", "class_gallery_1_1_repository_1_1_person_repository.html#a28c39268e5af887443f55ad9d3150e7d", null ]
];