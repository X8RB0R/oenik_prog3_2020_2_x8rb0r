var class_gallery_1_1_logic_1_1_person_logic =
[
    [ "PersonLogic", "class_gallery_1_1_logic_1_1_person_logic.html#a6a58e26673a671ac7bc83c7f40e68922", null ],
    [ "AddPerson", "class_gallery_1_1_logic_1_1_person_logic.html#ad171bbc2ccfb8f3544564f556f26b041", null ],
    [ "ChangeEmail", "class_gallery_1_1_logic_1_1_person_logic.html#a520f1b3dd769ba1580858aa141ae14bf", null ],
    [ "ChangePhoneNumber", "class_gallery_1_1_logic_1_1_person_logic.html#a852c20ddb9b18bfc201da0856c24e851", null ],
    [ "ChangeZipCode", "class_gallery_1_1_logic_1_1_person_logic.html#a3bacbbc24b94a291c77514c4e7de7816", null ],
    [ "DeletePerson", "class_gallery_1_1_logic_1_1_person_logic.html#ae7d0c9fd8c2aab01dc2945734cb078c7", null ],
    [ "GetAllPeople", "class_gallery_1_1_logic_1_1_person_logic.html#a2f819c36b248a714ea845dd4755af2fa", null ],
    [ "GetPersonById", "class_gallery_1_1_logic_1_1_person_logic.html#aee8ba1fb6f70699d216424239a99548c", null ],
    [ "GmailUsers", "class_gallery_1_1_logic_1_1_person_logic.html#aa27a887d6f76fa9a4d915cf0e26b913f", null ],
    [ "GmailUsersAsync", "class_gallery_1_1_logic_1_1_person_logic.html#aa4a1478e9a291d2e092409bea8468e55", null ],
    [ "PersonExists", "class_gallery_1_1_logic_1_1_person_logic.html#acfb5338b6db90b71206c2781e7121012", null ]
];