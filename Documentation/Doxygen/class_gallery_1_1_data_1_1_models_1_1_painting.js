var class_gallery_1_1_data_1_1_models_1_1_painting =
[
    [ "Condition", "class_gallery_1_1_data_1_1_models_1_1_painting.html#ae5ef5bd3d28fee3093f17d8b675b8369", null ],
    [ "Exhibit", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a6ad746aa8df586ef65d72fbf14876955", null ],
    [ "ExhibitId", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a58e0fa2526ae54a7bbb8a5de24bd4631", null ],
    [ "ExhibitIdString", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a784737c07248833a102b6ac43d57ec28", null ],
    [ "Painter", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a302d088d9c31096614839ffc34e21337", null ],
    [ "PaintingId", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a51c44dfdf494cafaa6c2b205d482a2db", null ],
    [ "Person", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a1135ad99f2172c97359f714eff16b5aa", null ],
    [ "PersonId", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a866efc65411077174f2f349719482b13", null ],
    [ "PersonIdString", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a399c23dc3be6e5a35ffaa567856cfc3f", null ],
    [ "Title", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a961afff370b070d3f69c8de306312494", null ],
    [ "Value", "class_gallery_1_1_data_1_1_models_1_1_painting.html#ab81932c1bfb4324036c6a4538cd7edfa", null ],
    [ "YearPainted", "class_gallery_1_1_data_1_1_models_1_1_painting.html#a7687939d0bec62b4148a9e1f982a8583", null ]
];