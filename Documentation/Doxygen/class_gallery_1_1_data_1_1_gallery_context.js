var class_gallery_1_1_data_1_1_gallery_context =
[
    [ "GalleryContext", "class_gallery_1_1_data_1_1_gallery_context.html#afc7fa920e619f87cf5ddf39a9bbf554a", null ],
    [ "OnConfiguring", "class_gallery_1_1_data_1_1_gallery_context.html#a0bbf4ae6c0fae8ff8d3a1aa51b025c81", null ],
    [ "OnModelCreating", "class_gallery_1_1_data_1_1_gallery_context.html#aa7f07f3bd5070815f4d3d1a89287cbdb", null ],
    [ "Exhibitions", "class_gallery_1_1_data_1_1_gallery_context.html#a2c778e04de16b4d5b01332033f885844", null ],
    [ "Paintings", "class_gallery_1_1_data_1_1_gallery_context.html#a2c8edc2d3318b461b7107c776067aa97", null ],
    [ "People", "class_gallery_1_1_data_1_1_gallery_context.html#a4b69f5bc6dae734621d7285591abba94", null ]
];