var namespace_gallery_1_1_repository =
[
    [ "Exceptions", "namespace_gallery_1_1_repository_1_1_exceptions.html", "namespace_gallery_1_1_repository_1_1_exceptions" ],
    [ "ExhibitRepository", "class_gallery_1_1_repository_1_1_exhibit_repository.html", "class_gallery_1_1_repository_1_1_exhibit_repository" ],
    [ "GenericRepository", "class_gallery_1_1_repository_1_1_generic_repository.html", "class_gallery_1_1_repository_1_1_generic_repository" ],
    [ "IExhibitRepository", "interface_gallery_1_1_repository_1_1_i_exhibit_repository.html", "interface_gallery_1_1_repository_1_1_i_exhibit_repository" ],
    [ "IPaintingRepository", "interface_gallery_1_1_repository_1_1_i_painting_repository.html", "interface_gallery_1_1_repository_1_1_i_painting_repository" ],
    [ "IPersonRepository", "interface_gallery_1_1_repository_1_1_i_person_repository.html", "interface_gallery_1_1_repository_1_1_i_person_repository" ],
    [ "IRepository", "interface_gallery_1_1_repository_1_1_i_repository.html", "interface_gallery_1_1_repository_1_1_i_repository" ],
    [ "PaintingRepository", "class_gallery_1_1_repository_1_1_painting_repository.html", "class_gallery_1_1_repository_1_1_painting_repository" ],
    [ "PersonRepository", "class_gallery_1_1_repository_1_1_person_repository.html", "class_gallery_1_1_repository_1_1_person_repository" ]
];