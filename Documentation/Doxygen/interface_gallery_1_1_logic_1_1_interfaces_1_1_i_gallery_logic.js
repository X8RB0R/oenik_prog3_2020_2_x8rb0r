var interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic =
[
    [ "AddExhibit", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#ae5d998135a9b3aced9fade6c58ee735a", null ],
    [ "AddPainting", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#a2050987a680727c908394f1822e8fbaf", null ],
    [ "ChangeExhibitEntryFee", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#a12f713a89f0e2747bc3d3f2d6acfa582", null ],
    [ "ChangeExhibitRating", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#a5530197e2a945d4a26a82b3421165e1b", null ],
    [ "ChangePaintingCondition", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#a77062a45b0802ff9d98871631397ca2c", null ],
    [ "ChangePaintingValue", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#a91a1d6b726ccd0d8575508edb1d8cf6a", null ],
    [ "DeleteExhibit", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#ada539addd34635b4df6e30908027dc9d", null ],
    [ "DeletePainting", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#a17422213245427f81c602bc035ed3e4d", null ],
    [ "ExhibitExists", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#aa718d55a361a036bde7dcde80202f299", null ],
    [ "GetAllExhibits", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#afa43678d8b8d1cedbfa49b45b981831b", null ],
    [ "GetAllPaintings", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#aee6dad1682bba99827225d6934b8f1b6", null ],
    [ "GetExhibit", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#a8c3bc6a0cef98ae5a204a8eeb9d8961c", null ],
    [ "GetPainting", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#af19574b8bca877603b6ad869dd7fe70f", null ],
    [ "PaintingExists", "interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html#a262c214191cc49966052d58d84cb72d7", null ]
];