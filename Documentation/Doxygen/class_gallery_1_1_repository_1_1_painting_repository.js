var class_gallery_1_1_repository_1_1_painting_repository =
[
    [ "PaintingRepository", "class_gallery_1_1_repository_1_1_painting_repository.html#adf96de90614377bb94d5243c50b6acf2", null ],
    [ "AddPainting", "class_gallery_1_1_repository_1_1_painting_repository.html#a4ca6f10ac143edd69a102fae430af6ff", null ],
    [ "ChangeCondition", "class_gallery_1_1_repository_1_1_painting_repository.html#a7fc99d1fb228b960bfeaa7afa4cbedff", null ],
    [ "ChangeValue", "class_gallery_1_1_repository_1_1_painting_repository.html#a920caf2bf0753f8d1cba42ff65d2e4c6", null ],
    [ "DeletePainting", "class_gallery_1_1_repository_1_1_painting_repository.html#ac27a2667938527c2c21f36f9ba372dfd", null ],
    [ "Exists", "class_gallery_1_1_repository_1_1_painting_repository.html#a972818f7f25ca3d237e03a814c67c69f", null ],
    [ "GetOne", "class_gallery_1_1_repository_1_1_painting_repository.html#aaf6b5a7e5d24853e00b433c6f2d89f5e", null ]
];