var class_gallery_1_1_repository_1_1_exhibit_repository =
[
    [ "ExhibitRepository", "class_gallery_1_1_repository_1_1_exhibit_repository.html#ad4c9140784e2908991fafbfbabf6a7d5", null ],
    [ "AddExhibit", "class_gallery_1_1_repository_1_1_exhibit_repository.html#ab752378ef0463ffdd1823d3357df23f3", null ],
    [ "ChangeEntryFee", "class_gallery_1_1_repository_1_1_exhibit_repository.html#a6017b6e5fbf110dfdf3ff6cb454379dd", null ],
    [ "ChangeRating", "class_gallery_1_1_repository_1_1_exhibit_repository.html#a3e7c74253ab48552e1727104b912835c", null ],
    [ "DeleteExhibit", "class_gallery_1_1_repository_1_1_exhibit_repository.html#ac0990bc84d1e96936339a13d49b3d135", null ],
    [ "Exists", "class_gallery_1_1_repository_1_1_exhibit_repository.html#ada06c4e044bee45d4264d8743f3249a0", null ],
    [ "GetOne", "class_gallery_1_1_repository_1_1_exhibit_repository.html#a624c1fafc18347bd73e4fdf4c6890ef3", null ]
];