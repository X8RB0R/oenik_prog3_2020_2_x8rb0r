var class_gallery_1_1_data_1_1_models_1_1_person =
[
    [ "Person", "class_gallery_1_1_data_1_1_models_1_1_person.html#adefa359d8ec73014e63e51182a9f1fad", null ],
    [ "BirthYear", "class_gallery_1_1_data_1_1_models_1_1_person.html#a8eaf0ffaf93d47d4aacd6b1b78747eeb", null ],
    [ "Email", "class_gallery_1_1_data_1_1_models_1_1_person.html#a27abbb0f564f646d3d86368e35d130f3", null ],
    [ "Name", "class_gallery_1_1_data_1_1_models_1_1_person.html#ab1e08404593d20a930ea331eb95c0947", null ],
    [ "Paintings", "class_gallery_1_1_data_1_1_models_1_1_person.html#aea018cc1066fd1583dc7dd5bd0fa3570", null ],
    [ "PersonId", "class_gallery_1_1_data_1_1_models_1_1_person.html#a7de22ebb34bcaf670fd8561841c97f0b", null ],
    [ "PhoneNumber", "class_gallery_1_1_data_1_1_models_1_1_person.html#ab8fea75b47db57987531d922006e6b65", null ],
    [ "ZipCode", "class_gallery_1_1_data_1_1_models_1_1_person.html#a469b3eaf816906b3655bd72cc64d2d12", null ]
];