var searchData=
[
  ['iexhibitrepository_74',['IExhibitRepository',['../interface_gallery_1_1_repository_1_1_i_exhibit_repository.html',1,'Gallery::Repository']]],
  ['igallerylogic_75',['IGalleryLogic',['../interface_gallery_1_1_logic_1_1_interfaces_1_1_i_gallery_logic.html',1,'Gallery::Logic::Interfaces']]],
  ['invalidchangeexception_76',['InvalidChangeException',['../class_gallery_1_1_repository_1_1_exceptions_1_1_invalid_change_exception.html',1,'Gallery.Repository.Exceptions.InvalidChangeException'],['../class_gallery_1_1_repository_1_1_exceptions_1_1_invalid_change_exception.html#a191681941cb427995e30e3d9e094fcc5',1,'Gallery.Repository.Exceptions.InvalidChangeException.InvalidChangeException(string message)'],['../class_gallery_1_1_repository_1_1_exceptions_1_1_invalid_change_exception.html#a368d6726b9d3b280368bf36b3a3ed862',1,'Gallery.Repository.Exceptions.InvalidChangeException.InvalidChangeException(string message, Exception innerException)'],['../class_gallery_1_1_repository_1_1_exceptions_1_1_invalid_change_exception.html#a1e3327c871482df928784f5e4ff069b8',1,'Gallery.Repository.Exceptions.InvalidChangeException.InvalidChangeException()']]],
  ['ipaintingrepository_77',['IPaintingRepository',['../interface_gallery_1_1_repository_1_1_i_painting_repository.html',1,'Gallery::Repository']]],
  ['ipersonlogic_78',['IPersonLogic',['../interface_gallery_1_1_logic_1_1_i_person_logic.html',1,'Gallery::Logic']]],
  ['ipersonrepository_79',['IPersonRepository',['../interface_gallery_1_1_repository_1_1_i_person_repository.html',1,'Gallery::Repository']]],
  ['irepository_80',['IRepository',['../interface_gallery_1_1_repository_1_1_i_repository.html',1,'Gallery::Repository']]],
  ['irepository_3c_20exhibit_20_3e_81',['IRepository&lt; Exhibit &gt;',['../interface_gallery_1_1_repository_1_1_i_repository.html',1,'Gallery::Repository']]],
  ['irepository_3c_20painting_20_3e_82',['IRepository&lt; Painting &gt;',['../interface_gallery_1_1_repository_1_1_i_repository.html',1,'Gallery::Repository']]],
  ['irepository_3c_20person_20_3e_83',['IRepository&lt; Person &gt;',['../interface_gallery_1_1_repository_1_1_i_repository.html',1,'Gallery::Repository']]]
];
