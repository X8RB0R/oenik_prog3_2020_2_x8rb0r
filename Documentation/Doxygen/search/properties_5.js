var searchData=
[
  ['painter_241',['Painter',['../class_gallery_1_1_data_1_1_models_1_1_painting.html#a302d088d9c31096614839ffc34e21337',1,'Gallery::Data::Models::Painting']]],
  ['painting_242',['PAINTING',['../class_gallery_1_1_logic_1_1_query_groups_1_1_most_expensive_painting_and_its_exhibit_group.html#a1cd4ab56583ca643954bde1227f1e433',1,'Gallery::Logic::QueryGroups::MostExpensivePaintingAndItsExhibitGroup']]],
  ['paintingid_243',['PaintingId',['../class_gallery_1_1_data_1_1_models_1_1_painting.html#a51c44dfdf494cafaa6c2b205d482a2db',1,'Gallery::Data::Models::Painting']]],
  ['paintings_244',['Paintings',['../class_gallery_1_1_data_1_1_gallery_context.html#a2c8edc2d3318b461b7107c776067aa97',1,'Gallery.Data.GalleryContext.Paintings()'],['../class_gallery_1_1_data_1_1_models_1_1_exhibit.html#ae36febc6a754c53e4093e1770ddd8173',1,'Gallery.Data.Models.Exhibit.Paintings()'],['../class_gallery_1_1_data_1_1_models_1_1_person.html#aea018cc1066fd1583dc7dd5bd0fa3570',1,'Gallery.Data.Models.Person.Paintings()']]],
  ['people_245',['People',['../class_gallery_1_1_data_1_1_gallery_context.html#a4b69f5bc6dae734621d7285591abba94',1,'Gallery::Data::GalleryContext']]],
  ['person_246',['Person',['../class_gallery_1_1_data_1_1_models_1_1_painting.html#a1135ad99f2172c97359f714eff16b5aa',1,'Gallery::Data::Models::Painting']]],
  ['personid_247',['PersonId',['../class_gallery_1_1_data_1_1_models_1_1_painting.html#a866efc65411077174f2f349719482b13',1,'Gallery.Data.Models.Painting.PersonId()'],['../class_gallery_1_1_data_1_1_models_1_1_person.html#a7de22ebb34bcaf670fd8561841c97f0b',1,'Gallery.Data.Models.Person.PersonId()']]],
  ['personidstring_248',['PersonIdString',['../class_gallery_1_1_data_1_1_models_1_1_painting.html#a399c23dc3be6e5a35ffaa567856cfc3f',1,'Gallery::Data::Models::Painting']]],
  ['personl_249',['PersonL',['../class_gallery_1_1_program_1_1_factory.html#ade625386d9f4b928d79d33ab6b70f0b0',1,'Gallery::Program::Factory']]],
  ['phonenumber_250',['PhoneNumber',['../class_gallery_1_1_data_1_1_models_1_1_person.html#ab8fea75b47db57987531d922006e6b65',1,'Gallery::Data::Models::Person']]]
];
