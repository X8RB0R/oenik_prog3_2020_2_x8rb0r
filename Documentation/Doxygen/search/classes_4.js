var searchData=
[
  ['gallerycontext_127',['GalleryContext',['../class_gallery_1_1_data_1_1_gallery_context.html',1,'Gallery::Data']]],
  ['gallerylogic_128',['GalleryLogic',['../class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html',1,'Gallery::Logic::Classes']]],
  ['gallerylogictests_129',['GalleryLogicTests',['../class_gallery_1_1_logic_1_1_tests_1_1_gallery_logic_tests.html',1,'Gallery::Logic::Tests']]],
  ['genericrepository_130',['GenericRepository',['../class_gallery_1_1_repository_1_1_generic_repository.html',1,'Gallery::Repository']]],
  ['genericrepository_3c_20exhibit_20_3e_131',['GenericRepository&lt; Exhibit &gt;',['../class_gallery_1_1_repository_1_1_generic_repository.html',1,'Gallery::Repository']]],
  ['genericrepository_3c_20painting_20_3e_132',['GenericRepository&lt; Painting &gt;',['../class_gallery_1_1_repository_1_1_generic_repository.html',1,'Gallery::Repository']]],
  ['genericrepository_3c_20person_20_3e_133',['GenericRepository&lt; Person &gt;',['../class_gallery_1_1_repository_1_1_generic_repository.html',1,'Gallery::Repository']]]
];
