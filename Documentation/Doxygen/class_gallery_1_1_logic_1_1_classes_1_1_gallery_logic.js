var class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic =
[
    [ "GalleryLogic", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#abaec5700cff820765d81eda8984d67d9", null ],
    [ "AddExhibit", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#a361e09180bdd049908f2f33423901761", null ],
    [ "AddPainting", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#aee9d802fdc1606c39d6af153fdab916f", null ],
    [ "ChangeExhibitEntryFee", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#a6d21ea933dfb54d9fd54308d6fc9002e", null ],
    [ "ChangeExhibitRating", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#a68f1a42b7d125797d7b66fff507cd377", null ],
    [ "ChangePaintingCondition", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#aa3a93c5fff9dbeab84e2717c679ab9ab", null ],
    [ "ChangePaintingValue", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#a67b0c357858628988fbe677cc9cec83b", null ],
    [ "DeleteExhibit", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#a02a1ad986cfeb0c9ff72cad682e4015a", null ],
    [ "DeletePainting", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#acc8a11b111ec6600822171f735aabf17", null ],
    [ "ExhibitExists", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#af776b027fb5d4a0108e239fbd800e103", null ],
    [ "ExhibitsAndTheNumberofPaintingsItHas", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#af737ce8a5e8fe257318e02df6085f9dc", null ],
    [ "ExhibitsAndTheNumberofPaintingsItHasAsync", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#ace1ba32b54c43963a64aa9d674629f64", null ],
    [ "GetAllExhibits", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#ae5ac16ddd4a84b93435d04f7970efb9a", null ],
    [ "GetAllPaintings", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#a5d44cc822e156dd90bbaf9e79abb3979", null ],
    [ "GetExhibit", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#abae60cb628c0a1c89827171fb7f9879f", null ],
    [ "GetPainting", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#ac55b949caab81bc7f08b186b2dc96e54", null ],
    [ "MostExpensivePaintingAndItsExhibit", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#aa4be6ef21d218a19de800324d4bfb479", null ],
    [ "MostExpensivePaintingAndItsExhibitAsync", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#aa24302e71438b6ea8e265f66c3c61a4a", null ],
    [ "PaintingExists", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#a4b6c37a5db41726beafe8a32f5f93b31", null ],
    [ "PaintingsInSunshineExhibit", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#aa433c03676e3dd9389af2a3e9ac0e285", null ],
    [ "PaintingsInSunshineExhibitAsync", "class_gallery_1_1_logic_1_1_classes_1_1_gallery_logic.html#a8ac02ceada90f896cc1bc51013748e72", null ]
];