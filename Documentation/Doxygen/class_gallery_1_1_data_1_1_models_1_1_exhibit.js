var class_gallery_1_1_data_1_1_models_1_1_exhibit =
[
    [ "Exhibit", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#a65fefb56973305f4515d55f27d90cbc6", null ],
    [ "End", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#aecf3e9ea4cce2fcab66911cee8d6fca3", null ],
    [ "EndDateString", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#a756860d658dc3117f524b2095d0a39e8", null ],
    [ "EntryFee", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#a5903c8117524d27b5f70658487348037", null ],
    [ "ExhibitId", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#abedad1b7f7780e70e4970f2db7e62e11", null ],
    [ "Paintings", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#ae36febc6a754c53e4093e1770ddd8173", null ],
    [ "Rating", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#a8d9afbb4d8bf0a0d4ad32c33b9cf6e04", null ],
    [ "Start", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#a25e85b6d3df8f45b83f62a707229df5a", null ],
    [ "StartDateString", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#a6e29933be448990ba01a8dde1312e359", null ],
    [ "Title", "class_gallery_1_1_data_1_1_models_1_1_exhibit.html#a738feb4fccf46288b5c63e0947481f96", null ]
];