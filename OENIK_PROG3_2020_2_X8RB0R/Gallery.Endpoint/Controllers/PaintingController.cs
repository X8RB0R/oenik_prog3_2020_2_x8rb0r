using Gallery.Data.Models;
using Gallery.Logic.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;


namespace MovieDbApp.Endpoint.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PaintingController : ControllerBase
    {

        IGalleryLogic logic;

        public PaintingController(IGalleryLogic logic)
        {
            this.logic = logic;
        }

        [HttpGet]
        public IEnumerable<Painting> ReadAll()
        {
            return this.logic.GetAllPaintings();
        }

        [HttpGet("{id}")]
        public Painting Read(int id)
        {
            return this.logic.GetPainting(id);
        }

        [HttpPost]
        public void Create([FromBody] Painting value)
        {
            this.logic.AddPainting(value);
        }
        [HttpPut]
        public void Update([FromBody] Painting value)
        {
            this.logic.UpdatePainting(value);
        }


        /*     [HttpPut]
             public void UpdatePaintingCondition(int id, int condition)
             {
                 this.logic.ChangePaintingCondition(id, condition);
             }
             [HttpPut]
             public void UpdatePaintingValue(int id, int value)
             {
                 this.logic.ChangePaintingValue(id, value);
             }*/



        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            this.logic.DeletePainting(id);
        }
    }
}
