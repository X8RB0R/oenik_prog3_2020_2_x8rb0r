﻿// <copyright file="IGalleryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Gallery.Logic.Interfaces
{
    using System.Collections.Generic;
    using Gallery.Data.Models;

    /// <summary>
    /// Gallery logic interface that consists of painting and exhibit repos.
    /// </summary>
    public interface IGalleryLogic
    {
        /// <summary>
        ///  Changes rating value of exhibit based on ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of exhibit instance.</param>
        /// <param name="newRating">New fee of exhibit by Id.</param>
        void ChangeExhibitRating(int id, int newRating);

        /// <summary>
        /// Changes condition of painting based on ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        /// <param name="newCondition">Condition of painting based on Id.</param>
        void ChangePaintingCondition(int id, int newCondition);

        /// <summary>
        /// Returns a single exhibit instance by ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of exhibit instance.</param>
        /// <returns>Exhibit instance.</returns>
        Exhibit GetExhibit(int id);

        /// <summary>
        /// Returns a single painting instance by ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        /// <returns>Painting instance by Id.</returns>
        Painting GetPainting(int id);

        /// <summary>
        /// Adds new painting instance.
        /// </summary>
        /// <param name="newPainting">New painting instance.</param>
        void AddPainting(Painting newPainting);

        /// <summary>
        /// Adds new exhibit instance.
        /// </summary>
        /// <param name="newExhibit">New exhibit instance.</param>
        void AddExhibit(Exhibit newExhibit);

        /// <summary>
        /// Deletes exhibit instance.
        /// </summary>
        /// <param name="id">Id of exhibit.</param>
        void DeleteExhibit(int id);

        /// <summary>
        /// Deletes painting instance.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        void DeletePainting(int id);

        /// <summary>
        /// Changes value of painting based on ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        /// <param name="newValue">Value of painting based on Id.</param>
        void ChangePaintingValue(int id, int newValue);

        /// <summary>
        /// Changes entry fee of exhibit based on ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of exhibit instance.</param>
        /// <param name="newFee">New fee of exhibit by Id.</param>
        void ChangeExhibitEntryFee(int id, int newFee);

        /// <summary>
        /// Returns a list of all the paintings, forwarded to the repository.
        /// </summary>
        /// <returns>IList of paintings.</returns>
        IList<Painting> GetAllPaintings();

        /// <summary>
        /// Returns a list of all exhibits, forwarded to the repository.
        /// </summary>
        /// <returns>Ilist of all Exhibits.</returns>
        IList<Exhibit> GetAllExhibits();

        /// <summary>
        /// Checks if painting exists in table.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        /// <returns>True if painting exists, false if not.</returns>
        bool PaintingExists(int id);

        /// <summary>
        /// Checks if exhibit exists in table.
        /// </summary>
        /// <param name="id">Id of exhibit.</param>
        /// <returns>True if exhibit exists, false if not.</returns>
        bool ExhibitExists(int id);

        void UpdatePainting(Painting p);

        void UpdateExhibit(Exhibit x);
    }
}
