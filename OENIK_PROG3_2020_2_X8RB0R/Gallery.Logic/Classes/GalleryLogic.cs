﻿// <copyright file="GalleryLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace Gallery.Logic.Classes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Gallery.Data.Models;
    using Gallery.Logic.Interfaces;
    using Gallery.Logic.QueryGroups;
    using Gallery.Repository;

    /// <summary>
    /// Gallery logic class that consists of painting and exhibit repos.
    /// </summary>
    public class GalleryLogic : IGalleryLogic
    {
        private readonly IPaintingRepository paintingRepo;
        private readonly IExhibitRepository exhibitRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="GalleryLogic"/> class.
        /// </summary>
        /// <param name="paintingRepo">Painting repo instance.</param>
        /// <param name="exhibitRepo">Exhibit repo instance.</param>
        public GalleryLogic(IPaintingRepository paintingRepo, IExhibitRepository exhibitRepo)
        {
            this.paintingRepo = paintingRepo;
            this.exhibitRepo = exhibitRepo;
        }

        /// <summary>
        /// Lists all paintings displayed in the sunshine exhibit.
        /// </summary>
        /// <returns>Collection of paintings.</returns>
        public IList<PaintingsInSunshineGroup> PaintingsInSunshineExhibit()
        {
                var q = from t1 in this.paintingRepo.GetAll()
                        join t2 in this.exhibitRepo.GetAll() on t1.ExhibitId equals t2.ExhibitId
                        where t2.Title == "Sunshine"
                        select new PaintingsInSunshineGroup()
                        {
                            NAME = t1.Title,
                        };

                return q.ToList();
        }

        /// <summary>
        /// Lists all paintings displayed in the sunshine exhibit asynchronously.
        /// </summary>
        /// <returns>Collection of paintings.</returns>
        public Task<IList<PaintingsInSunshineGroup>> PaintingsInSunshineExhibitAsync()
        {
            return Task.Run(() => this.PaintingsInSunshineExhibit());
        }

        /// <summary>
        /// Lists all exhibits and their paintings.
        /// </summary>
        /// <returns>Collection of exhibits and paintings.</returns>
        public IList<ExhibitsAndCountPaintingsGroup> ExhibitsAndTheNumberofPaintingsItHas()
        {
                var q = from t1 in this.paintingRepo.GetAll()
                        join t2 in this.exhibitRepo.GetAll() on t1.ExhibitId equals t2.ExhibitId
                        group t2 by t2.Title into g
                        select new ExhibitsAndCountPaintingsGroup()
                        {
                            EXHIBIT = g.Key,
                            NUMBER_OF_PAINTINGS = g.Count(),
                        };
                return q.ToList();
        }

        /// <summary>
        /// Lists all exhibits and their paintings asynchronously.
        /// </summary>
        /// <returns>Collection of exhibits and paintings.</returns>
        public Task<IList<ExhibitsAndCountPaintingsGroup>> ExhibitsAndTheNumberofPaintingsItHasAsync()
        {
            return Task.Run(() => this.ExhibitsAndTheNumberofPaintingsItHas());
        }

        /// <summary>
        /// Most expensive painting and the exhibit it belongs to.
        /// </summary>
        /// <returns>Most expensive painting.</returns>
        public IList<MostExpensivePaintingAndItsExhibitGroup> MostExpensivePaintingAndItsExhibit()
        {
                var q = (from t1 in this.paintingRepo.GetAll()
                         join t2 in this.exhibitRepo.GetAll() on t1.ExhibitId equals t2.ExhibitId
                         orderby t1.Value descending
                         select new MostExpensivePaintingAndItsExhibitGroup()
                         {
                             EXHIBIT = t2.Title,
                             PAINTING = t1.Title,
                         }).Take(1);
                return q.ToList();
        }

        /// <summary>
        /// Most expensive painting and the exhibit it belongs to asynchronously.
        /// </summary>
        /// <returns>Most expensive painting.</returns>
        public Task<IList<MostExpensivePaintingAndItsExhibitGroup>> MostExpensivePaintingAndItsExhibitAsync()
        {
            return Task.Run(() => this.MostExpensivePaintingAndItsExhibit());
        }

        /// <summary>
        /// Adds new exhibit instance.
        /// </summary>
        /// <param name="newExhibit">New exhibit instance.</param>
        public void AddExhibit(Exhibit newExhibit)
        {
            this.exhibitRepo.AddExhibit(newExhibit);
        }

        /// <summary>
        /// Adds new painting instance.
        /// </summary>
        /// <param name="newPainting">New painting instance.</param>
        public void AddPainting(Painting newPainting)
        {
            this.paintingRepo.AddPainting(newPainting);
        }

        /// <summary>
        /// Changes entry fee of exhibit based on ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of exhibit instance.</param>
        /// <param name="newFee">New fee of exhibit by Id.</param>
        public void ChangeExhibitEntryFee(int id, int newFee)
        {
            this.exhibitRepo.ChangeEntryFee(id, newFee);
        }

        /// <summary>
        ///  Changes rating value of exhibit based on ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of exhibit instance.</param>
        /// <param name="newRating">New fee of exhibit by Id.</param>
        public void ChangeExhibitRating(int id, int newRating)
        {
            this.exhibitRepo.ChangeRating(id, newRating);
        }

        /// <summary>
        /// Changes condition of painting based on ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        /// <param name="newCondition">Condition of painting based on Id.</param>
        public void ChangePaintingCondition(int id, int newCondition)
        {
            this.paintingRepo.ChangeCondition(id, newCondition);
        }

        /// <summary>
        /// Changes value of painting based on ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        /// <param name="newValue">Value of painting based on Id.</param>
        public void ChangePaintingValue(int id, int newValue)
        {
            this.paintingRepo.ChangeValue(id, newValue);
        }

        /// <summary>
        /// Deletes exhibit instance.
        /// </summary>
        /// <param name="id">Id of exhibit.</param>
        public void DeleteExhibit(int id)
        {
            this.exhibitRepo.DeleteExhibit(id);
        }

        /// <summary>
        /// Deletes painting instance.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        public void DeletePainting(int id)
        {
            this.paintingRepo.DeletePainting(id);
        }

        /// <summary>
        /// Returns a list of all exhibits, forwarded to the repository.
        /// </summary>
        /// <returns>Ilist of all Exhibits.</returns>
        public IList<Exhibit> GetAllExhibits()
        {
            return this.exhibitRepo.GetAll().ToList();
        }

        /// <summary>
        /// Returns a list of all the paintings, forwarded to the repository.
        /// </summary>
        /// <returns>IList of paintings.</returns>
        public IList<Painting> GetAllPaintings()
        {
            return this.paintingRepo.GetAll().ToList();
        }

        /// <summary>
        /// Returns a single exhibit instance by ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of exhibit instance.</param>
        /// <returns>Exhibit instance.</returns>
        public Exhibit GetExhibit(int id)
        {
            return this.exhibitRepo.GetOne(id);
        }

        /// <summary>
        /// Returns a single painting instance by ID, forwarded to the repository.
        /// </summary>
        /// <param name="id">Id of painting.</param>
        /// <returns>Painting instance by Id.</returns>
        public Painting GetPainting(int id)
        {
            return this.paintingRepo.GetOne(id);
        }

        /// <summary>
        /// Checks if painting of requested ID exists in table.
        /// </summary>
        /// <param name="id">ID of painting.</param>
        /// <returns>True if exists, false if not.</returns>
        public bool PaintingExists(int id)
        {
            return this.paintingRepo.Exists(id);
        }

        /// <summary>
        /// Checks if the exhibit of requested ID exists in the table.
        /// </summary>
        /// <param name="id">Id of exhibit.</param>
        /// <returns>True if exhibit exhists, false if not.</returns>
        public bool ExhibitExists(int id)
        {
            return this.exhibitRepo.Exists(id);
        }

        /// <summary>
        /// Update exhibit.
        /// </summary>
        /// <param name="newX">new exhibit.</param>
        public void UpdateExhibit(Exhibit newX)
        {
            this.exhibitRepo.UpdateExhibit(newX);
        }

        /// <summary>
        /// Update painting.
        /// </summary>
        /// <param name="newP">new painting.</param>
        public void UpdatePainting(Painting newP)
        {
            this.paintingRepo.UpdatePainting(newP);
        }
    }
}
